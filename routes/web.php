<?php

use App\Http\Controllers\Finger\FingerController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//  SELECT custid,max(orderdate)
//
//  FROM [TSQLV4].[Sales].[Orders]
//  where custid in (select distinct custid from Sales.Orders )
//  group by custid
//Route::get('/base',function (){
//
//    return DB::table('finger')
//        ->select('ID',
//            DB::raw('MIN(date) as date'),
//            DB::raw('MIN(time) as time'),
//            DB::raw('MIN(person) as person'),
//            DB::raw('MIN(direction) as direction'),
//            DB::raw('MIN(devicename) as devicename'))
//        ->where('date','=',date('Y-m-d'))
//        ->orderBy('time','desc')
//        ->groupBy('ID')
//        ->get();
//});
Route::get('/apii', function (){
    return DB::table('attlog')->get();
});
Route::get('/table', [FingerController::class,'index'])->name('index');
Route::get('/search',[FingerController::class,'search'])->name('search');

Route::get('/', function (){
    return view('fingerprint.index');
})->name('main');

Route::get('/test/bekhruz', function (){
    dd(DB::table('attlog')->get());
});
