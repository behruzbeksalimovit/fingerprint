<?php

namespace App\Http\Controllers\Finger;

use App\Http\Controllers\Controller;
use DateTime;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FingerController extends Controller
{
    /**
     * @var
     */
    public $departments;

    /**
     * @throws GuzzleException
     */
    public function __construct()
    {
        $client = new \GuzzleHttp\Client();
        $this->departments = json_decode($client->request('POST', 'https://uniwork.buxdu.uz/api/departments.asp')->getBody())->departments;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $inputs = DB::table('attlog')
            ->select('employeeID',
                DB::raw('MIN(Date) as date'),
                DB::raw('MIN(Time) as time'),
                DB::raw('MIN(personName) as person'),
                DB::raw('MIN(direction) as direction'),
                DB::raw('MIN(deviceName) as devicename'))
            ->where('date','=',date('Y-m-d'))
            ->where('direction', '=', 'IN')
            ->orderBy('time','asc')
            ->groupBy('employeeID')
            ->get();

        $outputs = DB::table('attlog')
            ->select('employeeID',
                DB::raw('MIN(Date) as date'),
                DB::raw('MIN(Time) as time'),
                DB::raw('MIN(personName) as person'),
                DB::raw('MIN(direction) as direction'),
                DB::raw('MIN(deviceName) as devicename'))
            ->where('date','=',date('Y-m-d'))
            ->where('direction', '=', 'OUT')
            ->orderBy('time','asc')
            ->groupBy('employeeID')
            ->get();

        return view('fingerprint.table', [
            'inputs' => $inputs,
            'outputs' => $outputs,
            'departments' => $this->departments
        ]);
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function search(Request $request)
    {
        if (isset($_GET['date'])) {
            $search = $_GET['date'];
            $search_rahbar_id = $_GET['rahbar_id'];
            $inputs = DB::table('attlog')
                ->select('employeeID',
                    DB::raw('MIN(Date) as date'),
                    DB::raw('MIN(Time) as time'),
                    DB::raw('MIN(personName) as person'),
                    DB::raw('MIN(direction) as direction'),
                    DB::raw('MIN(deviceName) as devicename'))
                ->orWhere('employeeID','=',$search_rahbar_id)
                ->where('Date','=',$search)
                ->where('direction', '=', 'IN')
                ->orderBy('time','asc')
                ->groupBy('employeeID')
                ->get();

//            $inputs->appends($request->all());

            $outputs = DB::table('attlog')
                ->select('employeeID',
                    DB::raw('MIN(Date) as date'),
                    DB::raw('MIN(Time) as time'),
                    DB::raw('MIN(personName) as person'),
                    DB::raw('MIN(direction) as direction'),
                    DB::raw('MIN(deviceName) as devicename'))
                ->orWhere('employeeID','=',$search_rahbar_id)
                ->where('date','=',$search)
                ->where('direction', '=', 'OUT')
                ->orderBy('time','asc')
                ->groupBy('employeeID')
                ->get();

            return view('fingerprint.search', [
                'inputs' => $inputs,
                'outputs' => $outputs,
                'departments' => $this->departments,
                'rahbar_id' => $search_rahbar_id
            ]);
        } else {
            return view('fingerprint.search');
        }

    }
}
