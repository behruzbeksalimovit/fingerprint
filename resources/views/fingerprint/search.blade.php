<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('fingerprint/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('fingerprint/css/index.css') }}">
    <link rel="icon" href="{{ asset('fingerprint/svg/favicon.ico') }}">
    <title>FingerPrint BuxDU</title>
</head>

<body class="body-tables">
<div class="navbar navbar-table">
    <div class="container">
        <div class="img">
            <a href="{{ route('main') }}">
                <img style="max-width:80%;" src="{{ asset('fingerprint/Logo.png') }}" alt="BuxDU" class="img-fluid">
            </a>
        </div>
    </div>
</div>
@if(isset($inputs))
    <section class="fingerprint-table">
    <div class="container py-5">
        <h2>Foydalanuvchilar jadvali</h2>
        <div class="fingerprint-table-parent">
            <div class="container">
                <div class="row">
                    <div class="d-flex align-items-center justify-content-between table-top">
                        <form class="form-group d-flex align-items-center justify-content-between w-100" method="get" action="{{ route('search') }}">
                            <div class="col-md-3">
                                <div class="calendar">
                                    <input type="date" name="date" class="form-control" @if(isset($inputs[0])) value="{{ $inputs[0] ? $inputs[0]->date : '' }}" @endif>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="select">
                                    <select class="form-control" name="rahbar_id" id="rahbar_id">
                                        @foreach($departments as $department)
                                            <option value="{{ $department->rahbar_id }}">{{ $department->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="search text-center">
                                    <button type="submit" class="text-center">Sana bo'yicha qidirish</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="text-align: center" rowspan="3">F.I.O</th>
                    <th style="text-align: center" rowspan="3">Qurilma</th>
                    <th style="text-align: center" colspan="2" rowspan="2">Vaqt</th>
                </tr>
                <tr></tr>
                <tr>
                    <th class="text-center">Kirish <br>7:30 - 13:30</th>
                    <th class="text-center">Chiqish <br>16:30 - 22:00</th>
                    <th class="text-center">Ishlagan vaqti <br> (1 soat tushlik uchun.)</th>
                </tr>
                </thead>

                <tbody>
                @foreach($inputs as $input)
                    <tr class="old-data">
                        <td class="subject">{{ $input->person }}</td>
                        <td>{{ $input->devicename }}</td>
                        @if(substr($input->time,0,5) >= "07:30" && substr($input->time,0,5) <= "13:30")
                            <td class="total text-center access">{{ substr($input->time,0,5) }}</td>
                        @else
                            <td class="total text-center exit">{{ substr($input->time,0,5)}}</td>
                        @endif
                        @foreach($outputs as $output)
                            @if($input->employeeID == $output->employeeID)
                                @if(substr($output->time,0,5) >= "16:30" && substr($output->time,0,5) <= "22:00")
                                    <td class="total text-center access">{{ substr($output->time,0,5) }}</td>
                                @else
                                    <td class="total text-center exit">{{ substr($output->time,0,5) }}</td>
                                @endif
                                @break
                            @endif
                        @endforeach

                        @foreach($outputs as $output)
                            @if($input->employeeID == $output->employeeID)
                                @if(substr($output->time,0,5) >= "16:30" && substr($output->time,0,5) <= "22:00")
                                    <td class="total text-center warning"> {{ gmdate('H', strtotime(substr($output->time,0,5)) - strtotime(substr($input->time,0,5))) - 1 }} soat {{ gmdate('i', strtotime(substr($output->time,0,5)) - strtotime(substr($input->time,0,5))) }} minut</td>
                                @endif
                                @break
                            @endif
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    </div>
</section>
@endif
</body>

</html>
